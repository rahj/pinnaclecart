<?php

namespace Pinnaclecart\Systems;

class View
{
    public function __construct()
    {

    }

    public function render($view)
    {
        $path = APP_PATH . 'Views/' . $view . '.php';
        if (file_exists($path)) {
            include_once($path);
        }
    }

    public function getHeader()
    {
        $path = APP_PATH . 'Views/Header/index.php';
        if (file_exists($path)) {
            include_once($path);
        }
    }

    public function getFooter()
    {
        $path = APP_PATH . 'Views/Footer/index.php';
        if (file_exists($path)) {
            include_once($path);
        }
    }

    public function getPageTitle()
    {
        $pageTitle = 'PinnacleCart Test by: Reynaldo A. Hipolito';
        return $pageTitle;
    }

    public function getDefaultCss()
    {
        $path = PUBLIC_ABSOLUTE_PATH . 'Css/default.css';
        $link = "<link type='text/css' rel='stylesheet' href='{$path}'>";
        return $link;
    }

    public function getBootstrap()
    {
        $css = "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7' crossorigin='anonymous'>";
        $js  = "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js' integrity='sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS' crossorigin='anonymous'></script>";
        $bootstrap = $css . $js;
        return $bootstrap;
    }

    public function getJquery()
    {
        $path   = PUBLIC_ABSOLUTE_PATH . 'Js/jquery.js';
        $script = "<script type='text/javascript' src='{$path}'></script>";
        return $script;
    }

    public function getDefaultJs()
    {
        $path   = PUBLIC_ABSOLUTE_PATH . 'Js/default.js';
        $script = "<script type='text/javascript' src='{$path}'></script>";
        return $script;
    }
}