<?php

namespace Pinnaclecart\Systems;

use Pinnaclecart\Systems\View;

abstract class AbstractController
{
    protected $view;
    protected $model;

    public function __construct()
    {
        $this->view = new View();
    }
}