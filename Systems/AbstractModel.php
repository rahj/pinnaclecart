<?php

namespace Pinnaclecart\Systems;

class AbstractModel
{
    protected $db;

    public function __construct()
    {
        $db_host = DB_HOST;
        $db_name = DB_NAME;
        $db_user = DB_USER;
        $db_pass = DB_PASS;

        try {
            $this->db = new \PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, array(\PDO::ATTR_PERSISTENT => true));
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}