<?php

if (!empty($_GET['url'])) {
    $url = explode('/', rtrim($_GET['url'], '/'));
    $url_count = count($url);

    /**
     * Controller Mapper
     */
    $controllerMapper = array(
        'Index' => 'Pinnaclecart\Applications\Controllers\Index',
    );

    /**
     * Path: domain/folder/controller|file/method/id
     */
    switch($url_count) {
        case 1:
            $url_0 = ucfirst($url[0]);
            $controller = new $controllerMapper[$url_0]();
            $controller->index();
            break;
        case 2:
            $url_1 = ucfirst($url[1]);
            $controller = new $controllerMapper[$url_1]();
            $controller->index();
            break;
        case 3:
            $url_1 = ucfirst($url[1]);
            $url_2 = $url[2];
            $controller = new $controllerMapper[$url_1]();
            $controller->$url_2();
            break;
        case 4:
            $url_1 = ucfirst($url[1]);
            $url_2 = $url[2];
            $url_3 = $url[3];
            $controller = new $controllerMapper[$url_1]();
            $controller->$url_2($url_3 = null);
            break;
        default:
            if ($url_count > 1) {
                $url_1 = ucfirst($url[1]);
                $controller = new $controllerMapper[$url_1]();
                $controller->index();
            } else {
                $url_0 = ucfirst($url[0]);
                $controller = new $controllerMapper[$url_0]();
                $controller->index();
            }
            break;
    }
} else {
    $index = new Pinnaclecart\Applications\Controllers\Index();
    $index->index();
}