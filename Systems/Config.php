<?php

/**
 * Set the database credentials
 */
define('DB_HOST', 'localhost');
define('DB_NAME', 'pinnaclecart');
define('DB_USER', 'root');
define('DB_PASS', '');

/**
 * Set the protocol
 */
define('PROTOCOL', 'http://');

/**
 * Set the base domain
 */
define('BASE_DOMAIN', PROTOCOL . $_SERVER['HTTP_HOST'] . '/');

/**
 * Set the document root
 */
define('BASE_PATH', $_SERVER['DOCUMENT_ROOT'] . '/');
define('APP_PATH', BASE_PATH . 'Applications/');
define('SYS_PATH', BASE_PATH . 'Systems/');
define('PUBLIC_ABSOLUTE_PATH', BASE_DOMAIN . 'Applications/Public/');

/**
 * Set the Autoloading
 */
function myAutoload($class) {
    $explode = explode('\\', $class);
    array_shift($explode);
    $path = BASE_PATH . implode('/', $explode) . '.php';

    if (file_exists($path)) {
        include_once($path);
    }
}

/**
 * Register to SPL_AUTOLOAD_REGISTER
 */
spl_autoload_register('myAutoload');