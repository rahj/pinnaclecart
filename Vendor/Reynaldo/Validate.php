<?php
/**
 * Validate Component by: Reynaldo
 */
namespace Pinnaclecart\Vendor\Reynaldo;

class Validate
{
    public function __construct() {}

    public function email($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
}