<?php
/**
 * Sanitization Component by: Reynaldo
 */
namespace Pinnaclecart\Vendor\Reynaldo;

class Sanitize
{
    public function __construct() {}

    public function string($string)
    {
        $string = filter_var($string, FILTER_SANITIZE_STRING);
        return $string;
    }

    public function integer($integer)
    {
        $integer = filter_var($integer, FILTER_SANITIZE_NUMBER_INT);
        return $integer;
    }

    public function email($email)
    {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        return $email;
    }

    public function url($url)
    {
        $url = filter_var($url, FILTER_SANITIZE_URL);
        return $url;
    }
}