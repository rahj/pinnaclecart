/**
 * Author: Reynaldo A. Hipolito
 * Email: rahj_1986@yahoo.com
 * Date: July 24, 2016
 * Desc: PinnacleCart Test
 */


1. For local environment, i use XAMPP 3.2.1

2. Set the Windows Hosts and Apache HTTPD Vhost

- windows: Windows\System32\Drivers\etc\hosts

127.0.0.1 pinnaclecart.localhost localhost


- Apache HTTPD Vhost: C:\xampp\conf\extra\httpd-vhosts
<VirtualHost *:80>
   ServerName pinnaclecart.localhost
   DocumentRoot "C:\xampp\htdocs\pinnaclecart"
   <Directory "C:\xampp\htdocs\pinnaclecart">
       AllowOverride All
       Require all granted
   </Directory>
</VirtualHost>


3. Importing Database from MySQL Dump

4. Database Credentials

-host: localhost
-db name: pinnaclecart
-db user: root
-db pass: 


5. IDE used: PhpStorm 10.0.3


6. BitBucket - https://rahj@bitbucket.org/rahj/pinnaclecart.git
