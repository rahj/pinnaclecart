<?php

namespace Pinnaclecart\Applications\Controllers;

use Pinnaclecart\Applications\Models\IndexModel;
use Pinnaclecart\Systems\AbstractController;
use Pinnaclecart\Vendor\Reynaldo\Sanitize;
use Pinnaclecart\Vendor\Reynaldo\Validate;

class Index extends AbstractController
{
    protected $sanitize;
    protected $validate;

    public function __construct()
    {
        parent::__construct();
        $this->sanitize = new Sanitize();
        $this->validate = new Validate();
        $this->model    = new IndexModel();
    }

    public function index()
    {
        $this->view->getHeader();
        $this->view->render('Index/index');
        $this->view->getFooter();
    }

    public function formsubmit()
    {
        $postName  = isset($_POST['postName']) ? $this->sanitize->string(trim($_POST['postName'])) : false;
        $postEmail = isset($_POST['postEmail']) ? $this->sanitize->email(trim($_POST['postEmail'])) : false;

        if (strlen($postName) > 0 && strlen($postEmail) > 0) {
            if (strlen($postName) <= 50 && strlen($postEmail) <= 50) {
                if ($this->validate->email($postEmail)) {
                    try {
                        if ($this->model->setUser($postName, $postEmail)) {
                            echo 1;
                            exit();
                        } else {
                            echo "Something went wrong when saving your data";
                            exit();
                        }
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                        exit();
                    }
                } else {
                    //email is not valid
                    echo 'Email is not valid';
                    exit();
                }
            } else {
                //some fields are more than 50 characters long
                echo 'Some fields are more than 50 characters long';
                exit();
            }
        } else {
            //some fields are empty
            echo 'Some fields are empty';
            exit();
        }
    }
}