/**
 * Author: Reynaldo A. Hipolito
 * Email: rahj_1986@yahoo.com
 * Date: July 24, 2016
 * Desc: PinnacleCart Test
 */
$(document).ready(function() {
    $(document).on('submit', '#myForm', function(e) {
        e.preventDefault();
        var formAction = $('#myForm').prop('action');
        var datas = $('#myForm').serialize();
        $('#button_submit').hide();
        $('#ajax_loader').show();

        $.ajax({
            type: 'POST',
            url: formAction,
            data: datas,
            success: function(return_data) {
                if (return_data == 1) {
                    name  = $('#postName').val();
                    email = $('#postEmail').val();
                    $('#alert_text').prop('class', 'alert alert-success').html("Thanks " + name + ", your email address " + email + " was added to subscribers list");
                    $('#myForm').trigger('reset');
                } else {
                    $('#alert_text').prop('class', 'alert alert-danger').html(return_data);
                }
                $('#ajax_loader').hide();
                $('#button_submit').show();
                setTimeout(function() {
                    $('#alert_text').fadeOut();
                }, 10000);
            },
            async: true
        });
    });
});