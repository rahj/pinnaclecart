<?php

namespace Pinnaclecart\Applications\Models;

use Pinnaclecart\Systems\AbstractModel;

class IndexModel extends AbstractModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setUser($name, $email)
    {
        $stmt1 = $this->db->prepare("
            SELECT * FROM users AS u
            WHERE
            u.email = '{$email}'
            LIMIT 1
        ");
        $stmt1->execute();
        $numrow1 = (int) $stmt1->rowCount();
        if ($numrow1 == 0) {
            $stmt2 = $this->db->prepare("
                INSERT INTO
                users (name, email)
                VALUES ('{$name}', '{$email}')
            ");
            $stmt2->execute();
            $numrow2 = (int) $stmt2->rowCount();
            if ($numrow2 == 1) {
                return true;
            }
        } else {
            throw new \Exception("User already exist in the Database");
        }
    }
}