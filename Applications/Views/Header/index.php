<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $this->getPageTitle(); ?></title>
    <?php
    echo $this->getJquery();
    echo $this->getBootstrap();
    echo $this->getDefaultCss();
    echo $this->getDefaultJs();
    ?>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="#" class="navbar-brand">PinnacleCart Test by: Reynaldo</a>
        </div>
        <div class="collapse navbar-collapse"></div>
    </div>
</nav>

<div id="main_wrapper">
    <div class="container">
        <div id="alert_wrapper"><div id="alert_text"></div></div>