
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div id="myForm_wrapper">
            <form action="<?php echo BASE_DOMAIN . 'index/index/formsubmit'; ?>" method="POST" id="myForm" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="postName">Name:</label>
                    <div class="col-sm-10">
                        <input type="text" id="postName" class="form-control" name="postName" placeholder="Enter Name" maxlength="50"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="postEmail">Email:</label>
                    <div class="col-sm-10">
                        <input type="text" id="postEmail" class="form-control" name="postEmail" placeholder="Enter Email" maxlength="50"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button id="button_submit" type="submit" class="btn btn-primary">Sign Up</button>
                        <div id="ajax_loader"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>

