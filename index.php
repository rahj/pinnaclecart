<?php
/**
 * Author: Reynaldo A. Hipolito
 * Email: rahj_1986@yahoo.com
 * Date: July 24, 2016
 * Desc: PinnacleCart Test
 */
namespace Pinnaclecart;

/**
 * Include the Config in systems
 */
include_once('Systems/Config.php');

/**
 * Include the Bootstrap in systems
 */
include_once('Systems/Bootstrap.php');